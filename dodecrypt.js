
function tryPassphrase(passphrase,silent) {
    console.log("tryPassphrase " + passphrase);
    var openpgp = window.openpgp;
    var options, encrypted;
    var encryptedMsg = document.getElementById('armored-content').innerHTML ;
    
    var armoredmsg = encryptedMsg.replace(/@/g, "\n");
    console.log("armoredmsg:\n" + armoredmsg);
    return openpgp.message.readArmored(armoredmsg).then(function (msg) {
        // console.log("msg " + JSON.stringify(msg));
        console.log("passphrase " + JSON.stringify(passphrase));
        options = {
            message: msg,
            passwords: [passphrase], // decrypt with password
            armor: true,
            format: 'utf8'
        };
        openpgp.decrypt(options).then(function(plaintext) {
            console.log("plaintext of msg " + JSON.stringify(plaintext));
            document.open("text/html", "replace");
            document.write(plaintext.data);
            document.close();
            return true;
        }).catch(function (error) {
            if (silent) {
                console.log("error: " + error);
            } else {
                var errors = document.getElementById('errors');
                errors.innerHTML = '<b>ERROR: decryption failed: </b><code>'
                    + error
                    + '</code> <i>(usually means wrong password …)</i>';
            };
        });
    });
};
console.log("Setting on submit");
document.getElementById('staticrypt-form').addEventListener('submit', function(e) {
    e.preventDefault();
    console.log("on the submit");
    var passphrase = document.getElementById('staticrypt-password').value;
    console.log("on the submit passph: " + passphrase);
    tryPassphrase(passphrase, false);
});
window.onload = function() { 
    var firstAttempt = "";
    if (window.location.hash.charAt(0) == '#') {
        firstAttempt = window.location.hash.substring(1);
    } else {
        firstAttempt = window.location.hash
    };
    tryPassphrase(firstAttempt, true).then(function(ret) {
        if (ret) {
            console.log("on the else of firstAttempt");
        } else {
            console.log("Nope from the hash: " + firstAttempt);
        };
    }, function(error) {
        console.log("Error from tryPassphrase: " + error);
    });
};
