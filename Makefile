

all: _build/hscrypt

_build/hscrypt: hscrypt dodecrypt.js
	mkdir -p _build && cat hscrypt dodecrypt.js > _build/hscrypt

BINDIR=~/bin
.PHONY:
install: _build/hscrypt
	mkdir -p $(BINDIR) && cp _build/hscrypt $(BINDIR)/hscrypt && chmod +x $(BINDIR)/hscrypt
